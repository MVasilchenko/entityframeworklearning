﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using EntityFrameworkCodeFirstExample.Entities;
using EntityFrameworkCodeFirstExample.Migrations;

namespace EntityFrameworkCodeFirstExample {
	public class SchoolContext : DbContext {
		public SchoolContext() : base("name=DefaultConnection") {
			Database.SetInitializer(new MigrateDatabaseToLatestVersion<SchoolContext, Configuration>("DefaultConnection"));
		}

		public DbSet<Student> Students { get; set; }
		public DbSet<Standard> Standards { get; set; }
		//public DbSet<StudentAddress> StudentAddresses { get; set; } 

		protected override void OnModelCreating( DbModelBuilder modelBuilder ) {
			Database.SetInitializer<SchoolContext>(new SchoolDbInitializer());
			base.OnModelCreating(modelBuilder);
		}
	}
}
 