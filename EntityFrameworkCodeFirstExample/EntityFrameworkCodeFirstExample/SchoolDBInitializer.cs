﻿using System.Collections.Generic;
using System.Data.Entity;
using EntityFrameworkCodeFirstExample.Entities;

namespace EntityFrameworkCodeFirstExample {
	public class SchoolDbInitializer : CreateDatabaseIfNotExists<SchoolContext> {
		protected override void Seed(SchoolContext context) {

			//context.Standards.Add(new Standard() { StandardName = "Standard 1", Description = "First Standard" });
			//context.Standards.Add(new Standard() { StandardName = "Standard 2", Description = "Second Standard" });
			//context.Standards.Add(new Standard() { StandardName = "Standard 3", Description = "Third Standard" });

			base.Seed(context);
		}
	}
}