﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using EntityFrameworkCodeFirstExample.Entities;

namespace EntityFrameworkCodeFirstExample {
	class Program {
		static void Main(string[] args) {
			using (var ctx = new SchoolContext())	{
				
				Create(ctx, "Vasiliy", 1);
				//Update(ctx, "Genadiy", 2);
				ctx.SaveChanges();
			}
		}

		static void Create( SchoolContext ctx, string studentName, int standardId ) {
			Student stud = new Student() { StudentName = studentName};
			var coursMath = new Course() {CourseName = "Math"};
			var coursBiology = new Course() {CourseName = "Biology"};
			var coursDrawing = new Course() {CourseName = "Drawing"};
			stud.Courses.Add(coursMath);
			stud.Courses.Add(coursBiology);
			stud.Courses.Add(coursDrawing);
			//stud.StdandardId = 1;

			var stud1 = new Student() {StudentName = "Jack"};
			coursMath.Students.Add(stud1);
			coursDrawing.Students.Add(stud1);
			coursBiology.Students.Add(stud1);
			//stud1.StdandardId = 1;

			ctx.Students.Add(stud);
			ctx.Students.Add(stud1);
		}
		static void Update( SchoolContext ctx, string studentName, int studentId) {
			var student = ctx.Students.First(s => s.StudentId == studentId);
			student.StudentName = studentName;
		}
	}
}
