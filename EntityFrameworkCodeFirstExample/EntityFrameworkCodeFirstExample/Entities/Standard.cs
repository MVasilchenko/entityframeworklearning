﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCodeFirstExample.Entities {
	public class Standard {
		public Standard() {
			Students = new List<Student>();
		}
		public string StandardName { get; set; }
		public int StandardId { get; set; }
		public string Description { get; set; }

		public virtual ICollection<Student> Students { get; set; }

	}
}
