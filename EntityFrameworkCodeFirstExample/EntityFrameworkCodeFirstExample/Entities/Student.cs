﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkCodeFirstExample.Entities {
	//	[Table("Brik")]
	public class Student {
		public Student() {
			Courses = new HashSet<Course>();
		}

		public int StudentId { get; set; }
		[Required]
		public string StudentName { get; set; }

		//public int? Age { get; set; }
		[Required]
		//public int StdandardId { get; set; }

		

		public virtual ICollection<Course> Courses { get; set; }
	}
}
